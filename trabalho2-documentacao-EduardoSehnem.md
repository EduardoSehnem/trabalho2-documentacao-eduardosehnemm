# Trabalho 2: Integração de habilidades - 2022/1
*Disciplina: Redes de Computadores*

*Curso: Engenharia de Computação / Elétrica*

*Nome/RA: Eduardo Sehnem - 2006545*


## Tarefa 1:  Sub-Redes
| Sub- Rede |             IPv6 - Sub-Rede            |  IPv4 - Sub-Rede  |  IPv4 - Máscara   | IPv4 - Broadcast  |    
|:---------:|:--------------------------------------:|:-----------------:|:-----------------:|:-----------------:|
| Matriz    | 2001:DB8:ACAD:2D00::/64 | 200.200.45.0   | 255.255.255.192 | 200.200.45.63  |
| Filial 1  | 2001:DB8:ACAD:2D01::/64 | 200.200.45.64  | 255.255.255.224 | 200.200.45.95  |
| Filial 2  | 2001:DB8:ACAD:2D02::/64 | 200.200.45.96  | 255.255.255.224 | 200.200.45.127 |
| Filial 3  | 2001:DB8:ACAD:2D03::/64 | 200.200.45.128 | 255.255.255.224 | 200.200.45.159 |
| Filial 4  | 2001:DB8:ACAD:2D04::/64 | 200.200.45.160 | 255.255.255.224 | 200.200.45.192 |
| Filial 5  | 2001:DB8:ACAD:2D05::/64 | 200.200.45.192 | 255.255.255.224 | 200.200.45.223 |
| pb-vit    | 2001:DB8:ACAD:2DFF::0:0/112 | 200.200.45.224 | 255.255.255.252 | 200.200.45.227 |
| vit-fb    | 2001:DB8:ACAD:2DFF::1:0/112 | 200.200.45.22 | 255.255.255.252 | 200.200.45.231 |
| fb-ita    | 2001:DB8:ACAD:2DFF::2:0/112 | 200.200.45.232 | 255.255.255.252 | 200.200.45.235 |
| ita-pb    | 2001:DB8:ACAD:2DFF::3:0/112 | 200.200.45.236 | 255.255.255.252 | 200.200.45.239 |
| cv-ita    | 2001:DB8:ACAD:2DFF::4:0/112  | 200.200.45.240 | 255.255.255.252 | 200.200.45.243 |


## Tarefa 2: Endereçamento de Dispositivos
| Dispositivo           | Interface | IPv4 | IPv4 - Máscara | IPv4 - Gateway | IPv6/Prefixo | IPv6 - Gateway |
|-----------------------|-----------|------|----------------|----------------|--------------|----------------|
| PC1 | NIC    |  200.200.45.3    |     255.255.255.192      |     200.200.45.1        |      2001:DB8:ACAD:2D00::3/64        |     2001:DB8:ACAD:2D00::1/64          |
| PC2 | NIC    |  200.200.45.4    |     255.255.255.192      |     200.200.45.1        |      2001:DB8:ACAD:2D00::4/64        |     2001:DB8:ACAD:2D00::1/64          |
| PC3 | NIC    |  200.200.45.67   |     255.255.255.224      |     200.200.45.65       |      2001:DB8:ACAD:2D01::3/64        |     2001:DB8:ACAD:2D01::1/64          |
| PC4 | NIC    |  200.200.45.68   |    255.255.255.224       |     200.200.45.65       |      2001:DB8:ACAD:2D01::4/64        |     2001:DB8:ACAD:2D01::1/64          |
| PC5 | NIC    |  200.200.45.99   |    255.255.255.224       |     200.200.45.97       |      2001:DB8:ACAD:2D02::3/64        |     2001:DB8:ACAD:2D02::1/64          |
| PC6 | NIC    |  200.200.45.100  |    255.255.255.224       |     200.200.45.97       |      2001:DB8:ACAD:2D02::4/64        |     2001:DB8:ACAD:2D02::1/64          |
| Switch-Matriz | SVI     |  200.200.45.2     |  255.255.255.192     |  200.200.45.1   |              |                |
| Switch-Filial1 | SVI    |  200.200.45.66    |  255.255.255.224     |  200.200.45.65  |              |                |
| Switch-Filial2 | SVI    |  200.200.45.98    |  255.255.255.224     |  200.200.45.97  |              |                |
| Roteador-Pato Branco  | Fa0/0   |  200.200.45.1      |    255.255.255.192   |                |   2001:DB8:ACAD:2D00::1/64            |                |
| Roteador-Pato Branco  | Se0/0/0 |  200.200.45.225    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::0:1/112         |                |
| Roteador-Pato Branco  | Se0/0/1 |  200.200.45.238    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::3:2/112         |                 |
| Roteador-Fco. Beltrão | Fa0/0   |  200.200.45.96     |    255.255.255.224   |                |   2001:DB8:ACAD:2D01::0:1/64            |                |
| Roteador-Fco. Beltrão | Se0/0/0 |  200.200.45.233    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::2:1/112         |                 |
| Roteador-Fco. Beltrão | Se0/0/1 |  200.200.45.230    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::1:2/112         |                 |
| Roteador-Vitorino     | Se0/0/0 |  200.200.45.229    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::1:1/112         |                 |
| Roteador-Vitorino     | Se0/0/1 |  200.200.45.226    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::0:2/112         |                 |
| Roteador-Itapejara    | Se0/0/0 |  200.200.45.237    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::3:1/112         |                 |
| Roteador-Itapejara    | Se0/0/1 |  200.200.45.234    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::2:2/112         |                 |
| Roteador-Itapejara    | Fa0/1   |  200.200.45.241    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::4:1/112         |                 |
| Roteador-Coronel      | Fa0/0   |  200.200.45.129     |    255.255.255.224   |                |   2001:DB8:ACAD:2D02::1/64            |                |
| Roteador-Coronel      | Fa0/1   |  200.200.45.242    |    255.255.255.252   |                |   2001:DB8:ACAD:2DFF::4:2/112         |                |


## Tarefa 3: Tabela de Roteamento
### Roteador Pato Branco
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
|    200.200.45.64              |  255.255.255.224       |   200.200.45.226       |      SE0/0/0              |
|    200.200.45.96              |  255.255.255.224       |   200.200.45.226       |      SE0/0/0              |
|    200.200.45.228             |  255.255.255.252       |   200.200.45.226       |      SE0/0/0              |
|    200.200.45.232             |  255.255.255.252       |   200.200.45.226       |      SE0/0/0              |
|    200.200.45.240             |  255.255.255.252       |   200.200.45.226       |      SE0/0/0              |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
|   2001:DB8:ACAD:2D01::/64                  |   2001:DB8:ACAD:2DFF::2       |      SE0/0/0              |
|   2001:DB8:ACAD:2D02::/64                  |   2001:DB8:ACAD:2DFF::2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::1:0/112              |   2001:DB8:ACAD:2DFF::2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::2:0/112              |   2001:DB8:ACAD:2DFF::2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::4:0/112              |   2001:DB8:ACAD:2DFF::2       |      SE0/0/0              |

### Roteador Francisco Beltrão
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
|    200.200.45.0              |  255.255.255.192       |   200.200.45.234       |      SE0/0/0              |
|    200.200.45.96             |  255.255.255.224       |   200.200.45.234       |      SE0/0/0              |
|    200.200.45.236            |  255.255.255.252       |   200.200.45.234       |      SE0/0/0              |
|    200.200.45.224            |  255.255.255.252       |   200.200.45.234       |      SE0/0/0              |
|    200.200.45.240            |  255.255.255.252       |   200.200.45.234       |      SE0/0/0              |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
|   2001:DB8:ACAD:2D00::/64                  |   2001:DB8:ACAD:2DFF::2:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2D02::/64                  |   2001:DB8:ACAD:2DFF::2:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::0:0/112              |   2001:DB8:ACAD:2DFF::2:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::3:0/112              |   2001:DB8:ACAD:2DFF::2:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::4:0/112              |   2001:DB8:ACAD:2DFF::2:2       |      SE0/0/0              |

### Roteador Vitorino
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
|    200.200.45.0              |  255.255.255.192       |   200.200.45.230       |      SE0/0/0              |
|    200.200.45.64             |  255.255.255.224       |   200.200.45.230       |      SE0/0/0              |
|    200.200.45.96             |  255.255.255.224       |   200.200.45.230       |      SE0/0/0              |
|    200.200.45.232            |  255.255.255.252       |   200.200.45.230       |      SE0/0/0              |
|    200.200.45.236            |  255.255.255.252       |   200.200.45.230       |      SE0/0/0              |
|    200.200.45.240            |  255.255.255.252       |   200.200.45.230       |      SE0/0/0              |


#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
|   2001:DB8:ACAD:2D00::/64                  |   2001:DB8:ACAD:2DFF::1:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2D01::/64                  |   2001:DB8:ACAD:2DFF::1:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2D02::/64                  |   2001:DB8:ACAD:2DFF::1:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::2:0/112              |   2001:DB8:ACAD:2DFF::1:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::3:0/112              |   2001:DB8:ACAD:2DFF::1:2       |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::4:0/112              |   2001:DB8:ACAD:2DFF::1:2       |      SE0/0/0              |

### Roteador Itapejara D'Oeste
#### IPv4
| Rede de Destino | Máscara | Next Hop | Interface de Saída |
|-----------------|---------|----------|--------------------|
|    200.200.45.0             |  255.255.255.192       |   200.200.45.238       |      SE0/0/0              |
|    200.200.45.64            |  255.255.255.224       |   200.200.45.238       |      SE0/0/0              |
|    200.200.45.96            |  255.255.255.224       |   200.200.45.242       |      Fa0/1                |
|    200.200.45.224           |  255.255.255.252       |   200.200.45.238       |      SE0/0/0              |
|    200.200.45.228           |  255.255.255.252       |   200.200.45.238       |      SE0/0/0              |

#### IPv6
| Rede de Destino/Prefixo | Next Hop | Interface de Saída |
|-----------------|----------|--------------------|
|   2001:DB8:ACAD:2D00::/64                  |   2001:DB8:ACAD:2DFF::3:2      |      SE0/0/0              |
|   2001:DB8:ACAD:2D01::/64                  |   2001:DB8:ACAD:2DFF::3:2      |      SE0/0/0              |
|   2001:DB8:ACAD:2D02::/64                  |   2001:DB8:ACAD:2DFF::4:2      |      Fa0/1                |
|   2001:DB8:ACAD:2DFF::0:0/112              |   2001:DB8:ACAD:2DFF::3:2      |      SE0/0/0              |
|   2001:DB8:ACAD:2DFF::1:0/112              |   2001:DB8:ACAD:2DFF::3:2      |      SE0/0/0              |



## Topologia - Packet Tracer
- [ ] ![Trabalho2-Topologia-EduardoSehnem](trabalho2-topologia-EduardoSehnem.pkt)


## Arquivos de Configuração dos Dispositivos Intermediários (roteadores e switches)
- [ ] ![Roteador Pato Branco](r-pb-es.txt)
- [ ] ![Roteador Francisco Beltrão](r-fb-es.txt)
- [ ] ![Roteador Vitorino](r-vit-es.txt)
- [ ] ![Roteador Itapejara D'Oeste](r-ita-es.txt)
- [ ] ![Roteador Coronel Vivida](r-cvv-es.txt)
- [ ] ![Switch Pato Branco](sw-matriz-es.txt)
- [ ] ![Switch Francisco Beltrão](sw-filial1-es.txt)
- [ ] ![Switch Coronel Vivida](sw-filial2-es.txt)
