!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
service password-encryption
security passwords min-length 10
!
hostname r-cvv-es
!
login block-for 120 attempts 3 within 60
!
!
enable secret 5 $1$mERr$wVtQEi940LmepLoRi38LB1
!
!
!
!
!
!
no ip cef
ipv6 unicast-routing
!
no ipv6 cef
!
!
!
username eduardo secret 5 $1$mERr$F5V/u2120qbEqnDJcxLe00
!
!
license udi pid CISCO2811/K9 sn FTX1017JL3K-
!
!
!
!
!
!
!
!
!
ip domain-name eduardo.sehnem.br
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 description Filial_2
 ip address 200.200.45.97 255.255.255.224
 duplex auto
 speed auto
 ipv6 address FE80::1 link-local
 ipv6 address 2001:DB8:ACAD:2D02::1/64
 ipv6 enable
!
interface FastEthernet0/1
 description ita-cvv
 ip address 200.200.45.242 255.255.255.252
 duplex auto
 speed auto
 ipv6 address 2001:DB8:ACAD:2DFF::4:2/112
 ipv6 enable
!
interface Serial0/0/0
 no ip address
 clock rate 2000000
 shutdown
!
interface Serial0/0/1
 no ip address
 clock rate 2000000
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 200.200.45.0 255.255.255.192 200.200.45.241 
ip route 200.200.45.64 255.255.255.224 200.200.45.241 
ip route 200.200.45.224 255.255.255.252 200.200.45.241 
ip route 200.200.45.228 255.255.255.252 200.200.45.241 
ip route 200.200.45.232 255.255.255.252 200.200.45.241 
ip route 200.200.45.236 255.255.255.252 200.200.45.241 
!
ip flow-export version 9
!
ipv6 route 2001:DB8:ACAD:2D00::/64 2001:DB8:ACAD:2DFF::4:1
ipv6 route 2001:DB8:ACAD:2D01::/64 2001:DB8:ACAD:2DFF::4:1
ipv6 route 2001:DB8:ACAD:2DFF::/112 2001:DB8:ACAD:2DFF::4:1
ipv6 route 2001:DB8:ACAD:2DFF::1:0/112 2001:DB8:ACAD:2DFF::4:1
ipv6 route 2001:DB8:ACAD:2DFF::2:0/112 2001:DB8:ACAD:2DFF::4:1
ipv6 route 2001:DB8:ACAD:2DFF::3:0/112 2001:DB8:ACAD:2DFF::4:1
!
ip access-list extended sl_def_acl
 deny tcp any any eq telnet
 deny tcp any any eq www
 deny tcp any any eq 22
 permit tcp any any eq 22
!
banner motd ^C
Roteador Coronel Vivida
ATENCAO Acesso Restrito a pessoas autorizadas!
Administrador: Eduardo Sehnem (eduardosehnem@alunos.utfpr.edu.br)
^C
!
!
!
!
!
line con 0
 exec-timeout 5 0
 password 7 08016F41070A4812161E0D162E24
 login
!
line aux 0
!
line vty 0 4
 exec-timeout 5 0
 login local
 transport input ssh
line vty 5 15
 exec-timeout 5 0
 login local
 transport input ssh
!
!
!
end